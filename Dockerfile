# This dockerfile is used only to run tests in. It should not be used to package
# the library.
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.12-alpine
WORKDIR /usr/src/app
ADD ./ ./
RUN \
	apk --no-cache add gcc g++ musl-dev libstdc++ libffi-dev libxml2-dev libxslt-dev && \
	pip install --no-cache-dir --upgrade -r ./requirements.txt && \
	pip install --no-cache-dir --upgrade tox
