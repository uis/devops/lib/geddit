import unittest
import unittest.mock as mock

from geddit import geddit


class GoogleStorageTestCase(unittest.TestCase):
    def setUp(self):
        client_patcher = mock.patch("google.cloud.storage.Client")
        self.mock_client_class = client_patcher.start()
        self.addCleanup(client_patcher.stop)

        self.mock_client = self.mock_client_class.return_value
        self.mock_content = b"some request content"
        self.mock_bucket = self.mock_client.bucket.return_value
        self.mock_blob = self.mock_bucket.get_blob.return_value
        self.mock_blob.download_as_bytes.return_value = self.mock_content

    def test_basic_fetch(self):
        """Can fetch a basic gs://... URL."""
        content = geddit("gs://my-bucket/path/to/object")
        self.mock_client.bucket.assert_called_with("my-bucket")
        self.mock_bucket.get_blob.assert_called_with("path/to/object")
        self.assertEqual(content, self.mock_content)
