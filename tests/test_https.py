import unittest
import unittest.mock as mock

from geddit import geddit


class HTTPSTestCase(unittest.TestCase):
    def setUp(self):
        get_patcher = mock.patch("requests.get")
        self.mock_get = get_patcher.start()
        self.addCleanup(get_patcher.stop)

        self.mock_content = b"some request content"
        self.mock_get.return_value.content = self.mock_content

    def test_basic_case(self):
        """A HTTPS URL ends up calling requests.get()."""
        url = "https://example.com/foo/bar"
        content = geddit(url)
        self.mock_get.assert_called_with(url)
        self.assertEqual(content, self.mock_content)

    def test_http_error(self):
        """raise_for_status() is called on the response"""
        url = "https://example.com/foo/bar"
        geddit(url)
        self.mock_get.return_value.raise_for_status.assert_called()

    def test_port(self):
        """A HTTPS URL with a port is supported."""
        url = "https://example.com:1234/foo/bar"
        content = geddit(url)
        self.mock_get.assert_called_with(url)
        self.assertEqual(content, self.mock_content)

    def test_authenticated(self):
        """A HTTPS URL with basic auth does not pass an auth param to requests.get()."""
        url = "https://user:pass@example.com/foo/bar"
        content = geddit(url)
        self.mock_get.assert_called_with(url)
        self.assertEqual(content, self.mock_content)
