import contextlib
import os
import tempfile
import unittest
import urllib.parse

from geddit import geddit


class FileTestCase(unittest.TestCase):
    def setUp(self):
        # Create a temporary directory and a file within it with known content
        with contextlib.ExitStack() as stack:
            self.temp_dir = stack.enter_context(tempfile.TemporaryDirectory(prefix="testing-"))
            self.addCleanup(stack.pop_all().close)
        self.file_path = os.path.join(self.temp_dir, "testing")
        self.content = b"hello"
        with open(self.file_path, "wb") as fobj:
            fobj.write(self.content)

    def test_default_scheme(self):
        """The default scheme is file."""
        self.assertEqual(geddit(self.file_path), self.content)

    def test_url(self):
        """Using a file:// URL also works."""
        url = urllib.parse.urlunsplit(("file", "", self.file_path, "", ""))
        self.assertTrue(url.startswith("file://"))
        self.assertEqual(geddit(url), self.content)

    def test_exception_propagation(self):
        """The underlying IOError is propagated if the file does not exist."""
        with self.assertRaises(IOError):
            geddit(self.file_path + "-which-does-not-exist")

    def test_relative_path(self):
        """Using a relative path raises ValueError."""
        with self.assertRaises(ValueError):
            geddit("some/relative/path")
