import unittest

from geddit import geddit


class DataTestCase(unittest.TestCase):
    def test_basic(self):
        self.assertEqual(
            geddit("data:text/plain;charset=UTF-8;page=21,the%20data:1234,5678"),
            b"the data:1234,5678",
        )

    def test_base64(self):
        self.assertEqual(geddit("data:text/vnd-example+xyz;foo=bar;base64,R0lGODdh"), b"GIF87a")

    def test_readme_example(self):
        self.assertEqual(
            geddit("data:application/x-yaml;charset=utf-8;base64,eyJmb28iOjF9"), b'{"foo":1}'
        )
