import unittest
import unittest.mock as mock

from geddit import geddit


class SecretManagerTestCase(unittest.TestCase):
    def setUp(self):
        client_patcher = mock.patch("google.cloud.secretmanager_v1.SecretManagerServiceClient")
        self.mock_client_class = client_patcher.start()
        self.addCleanup(client_patcher.stop)

        self.mock_client = self.mock_client_class.return_value
        self.mock_content = b"some request content"
        self.mock_client.access_secret_version.return_value.payload.data = self.mock_content

    def test_default_version(self):
        """Can fetch the default version."""
        content = geddit("sm://my-project/my-secret")
        self.mock_client.secret_version_path.assert_called_with(
            "my-project", "my-secret", "latest"
        )
        self.mock_client.access_secret_version.assert_called_with(
            name=self.mock_client.secret_version_path.return_value
        )
        self.assertEqual(content, self.mock_content)

    def test_explicit_version(self):
        """Can fetch an explicit version."""
        content = geddit("sm://my-project/my-secret#123")
        self.mock_client.secret_version_path.assert_called_with("my-project", "my-secret", "123")
        self.mock_client.access_secret_version.assert_called_with(
            name=self.mock_client.secret_version_path.return_value
        )
        self.assertEqual(content, self.mock_content)

    def test_bad_name(self):
        """Secret names cannot contain "/"."""
        with self.assertRaises(ValueError):
            geddit("sm://my-project/secrets/my-secret")

    def test_bad_version(self):
        """Secret versions cannot contain "/"."""
        with self.assertRaises(ValueError):
            geddit("sm://my-project/my-secret#version/1")
