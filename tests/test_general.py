import unittest

from geddit import geddit


class GeneralTestCase(unittest.TestCase):
    def test_unknown_scheme(self):
        """An unknown scheme raises ValueError."""
        with self.assertRaises(ValueError):
            geddit("gopher://gopher.example.com/archie-index")
