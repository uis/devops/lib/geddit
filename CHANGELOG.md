# Changelog

## [1.1.0](https://gitlab.developers.cam.ac.uk/uis/devops/lib/geddit/compare/1.0.3...1.1.0) (2024-10-03)


### Features

* configure release-it ([70271f8](https://gitlab.developers.cam.ac.uk/uis/devops/lib/geddit/commit/70271f81ed166d93da2e44cd19333444d4955681))


### Bug Fixes

* **deps:** update dependency google-cloud-storage to v2 ([744455d](https://gitlab.developers.cam.ac.uk/uis/devops/lib/geddit/commit/744455d5baef92c7928f2930638b20399b66af74))

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.3] - 2024-08-14
### Changed
 - Support Google Storage client libraries <3.0 instead of <2.0.

## [1.0.2] - 2024-03-04
### Changed
 - Use poetry for packaging and align code formatting to current lint standards.
 - Remove the need for requiring bucket permissions to use Google Storage.

## [1.0.1] - 2021-07-15
### Added
 - Support for data URLs.
### Changed
 - Replace use of deprecated download_as_string function for Google Storage.

## [1.0.0] - 2020-08-07
### Added
 - Initial version
